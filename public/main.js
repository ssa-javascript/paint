//html 태그 바인딩
const canvas = document.getElementById("jsCanvas");
const ctx = canvas.getContext("2d"); //랜더링 컨텍스트와 (렌더링 컨텍스트의) 그리기 함수들을 사용할 수 있음
const colors = document.getElementsByClassName("jsColor");
const range = document.getElementById("jsRange");
const mode = document.getElementById("jsMode");
const saveBtn = document.getElementById("jsSave");

//초기 캔버스 사이즈 설정
const CANVAS_SIZE = window.innerWidth/2;
canvas.width = CANVAS_SIZE;
canvas.height = CANVAS_SIZE;

ctx.fillStyle = "white"; //도형을 채우는 색을 설정합니다.
ctx.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE); //색칠된 직사각형을 그립니다.
const INITIAL_COLOR = "#2c2c2c";
//pen 색 설정
ctx.strokeStyle = INITIAL_COLOR; //도형의 윤곽선 색을 설정합니다.
ctx.fillStyle = INITIAL_COLOR;//도형을 채우는 색을 설정합니다.
ctx.lineWidth = 2.5;//이후 그려질 선의 두께를 설정합니다.
//ctx.lineCap=butt; // 끝점 모양을 결정 ,but,round,square



//동작 구분 변수 
let painting = false;
let filling = false;

function stopPainting() {
  painting = false;
}

function startPainting(event) {
  painting = true;
  if (event.type=="touchstart") {
    ctx.beginPath(); 
    ctx.moveTo(x, y); //펜을  x와 y 로 지정된 좌표로 옮깁니다.
  }
}

function onMouseMove(event) {
  
  let x = event.offsetX;
  let y = event.offsetY;
  if(event.type=="touchmove"){
     x = event.touches[0].pageX-canvas.offsetLeft;
     y = event.touches[0].pageY-canvas.offsetTop;
    }
console.log(painting+' 1 draw  '+ x +" "+y);
  if (!painting) {
    //새로운 경로를 만듭니다. 경로가 생성됬다면, 이후 그리기 명령들은 경로를 구성하고 만드는데 사용하게 됩니다.
    //마우스 포인트 움직일때마다  펜의 좌표를 마우스로 고정
    ctx.beginPath(); 
    ctx.moveTo(x, y); //펜을  x와 y 로 지정된 좌표로 옮깁니다.
  } else {
 
    ctx.lineTo(x, y);//현재의 드로잉 위치에서 x와 y로 지정된 위치까지 선을 그립니다.
    ctx.stroke(); //윤곽선을 이용하여 도형을 그립니다.
  }
}

function handleColorClick(event) {
  const color = event.target.style.backgroundColor;
  ctx.strokeStyle = color; //도형의 윤곽선 색을 설정합니다.
  ctx.fillStyle = color; //도형을 채우는 색을 설정합니다.
}

function handleRangeChange(event) {
  const size = event.target.value;
  ctx.lineWidth = size;
}

function handleModeClick() {
  if (filling === true) {
    filling = false;
    mode.innerText = "Fill";
  } else {
    filling = true;
    mode.innerText = "Paint";
  }
}

function handleCanvasClick() {
  if (filling) {
    ctx.fillRect(0, 0, CANVAS_SIZE, CANVAS_SIZE); //색칠된 직사각형을 그립니다.
  }
}

function handleCM(event) {
  event.preventDefault();// 원래 있던 오른쪽 마우스 이벤트를 무효화한다.
}

function handleSaveClick() {
  const image = canvas.toDataURL();
  const link = document.createElement("a");
  link.href = image;
  link.download = "Paint";
  link.click();
}

if (canvas) {
  canvas.addEventListener("mousemove", onMouseMove);
  canvas.addEventListener("mousedown", startPainting);
  canvas.addEventListener("mouseup", stopPainting);
  canvas.addEventListener("mouseleave", stopPainting);
  //toutch
  canvas.addEventListener("touchmove", onMouseMove);
  canvas.addEventListener("touchstart", startPainting);
  canvas.addEventListener("touchend", stopPainting);
  canvas.addEventListener("touchcancel", stopPainting);
  //click
  canvas.addEventListener("click", handleCanvasClick);
  canvas.addEventListener("contextmenu", handleCM); //contextmenu 우측 클릭 이벤트 
}
// Array.from() 메서드는 유사 배열 객체(array-like object)나반복 가능한 객체(iterable object)를 참조 복사해
// 새로운Array 객체를 만듭니다.
Array.from(colors).forEach(color =>
  color.addEventListener("click", handleColorClick)
);

if (range) {
  range.min=1;
  range.max=10;
  range.addEventListener("input", handleRangeChange);
}

if (mode) {
  mode.addEventListener("click", handleModeClick);
}

if (saveBtn) {
  saveBtn.addEventListener("click", handleSaveClick);
}